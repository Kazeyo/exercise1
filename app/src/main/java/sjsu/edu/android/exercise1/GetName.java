package sjsu.edu.android.exercise1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class GetName extends Activity implements View.OnClickListener{
    android.widget.EditText name;
    android.widget.Button button;
    @Override
    public void onClick(View view) {
        Intent myIntent = new Intent(this, MainActivity.class);
        myIntent.putExtra("uname", name.getText().toString());
        this.startActivity(myIntent);
    }

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_get_name);

        name = (EditText) this.findViewById(R.id.editTextTextPersonName);
        button = (Button) this.findViewById(R.id.button);
        button.setOnClickListener(this);
    }
}
